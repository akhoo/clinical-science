---
title: "Global infn"
author: "Aaron Khoo"
date: "`r format(Sys.Date(), '%d %B %Y')`"
output:
  pdf_document
documentclass: article
classoption: a4paper
fontsize: 11pt
urlcolor: blue
---

# Main case

## Trigger 1: Presentation

You are a GP in a Cairns practice. Your next patient is Richard Carter, a 26-year-old medical graduate who has recently returned from a 2-month position working as a volunteer on an international aid program in the Sudan.

Richard is clearly unwell, and says he started to feel ill five days ago - about 48 hours after he arrived home. It started with a fever and a severe headache. He says that he thought it was just the 'flu as a lot of people on the plane were coughing, but he has come in as he is still not well and has now developed a rash.

Febrile illness (2-3% of travellers)

-  Travel-related infn: malaria, infleunza, dengue, rickettsial infn, non-spec viral, bacterial diarrhoea, vaccine-preventable (Hep A, B, typhoid)
-  Travel-related non-infn: PE, drug reactions
-  Common infn
-  Malignancy
-  Autoimmune disorders

Common infn acquired by travellers (alt table)

-  Foodborne & waterborne: Hep A, Salmonella enterica causing typhoid, enteric pathogens (e.g. cholera)
-  Vector: malaria, dengue, Jap encephalitis, yellow fever, tick-borne encephalitis
-  Aerosol: influenza, meningococcal, measles, mumps, varicella, Tb
-  Bloodborne: Hep B, Hep C, HIV
-  Exotic: rabies, schistomiasis, leptospirosis

Most common:

-  Malaria: fever, malaise, nausea, vomiting, abdo pain, diarrhoea, myalgia, anaemia
-  Dengue: non-spec febrile illness or dengue haemorrhagic fever, headache, retro-orbital pain, myalgia, bone+joint pain, weakness, anorexia, taste aberrations, sore throat, cough, vomiting, abdo pain, itch
-  EBV mononucleosis
-  Rickettsial infn
-  Typhoid

Fever +

-  Resp: influenza, middle east respiratory syndrome coronavirus, Tb, legionnaires, leptospirosis, Q fever, histoplasmosis, hantavirus, plague, psittacosis, melioidosis, acute schistosomiasis syndrome, tropical pulmonary eosinophilia, loeffler syndrome, strongyloidiasis, fungal (coccidioidomycosis, paracoccidioidomycosis)
-  Jaundice: severe malaria/dengue, viral hep AE faecal-oral, hep BCD body fluids, leptospirosis, yellow fever, crimean-congo haemorrhagic fever, viruses (ebola, marburg, lassa), oroya fever
-  Abdo pain: enteric fever (Salmonella enterica serotype Typhi, S. enterica), liver abscess (Klebsiella), cholangitis due to liver fluke infection
-  Diarrhoea: enteropathies (Campylobacter, Shigella, Salmonella), cyclosporiasis, cyptosporidiosis, intestinal amebiasis, strongyloidiasis
-  Rash: dengue, chikungunya, zika, rickettsial infn, enteric, acute HIV, measles, meningococcal, schistomiasis, scrub typhus infection, Anthrax, rubella
-  Neuro: cerebral malaria, meningococcal, west nile, rabies, jap encephalitis, tickborne encephalitis, scrub typhus, east african sleeping sickness

-  What are the significant cues in this trigger?
    -  26M, medical, 2/12 Sudan, 5 days sick starting 2 days after he arrived home, rash
-  What aspects of Richard’s overseas travel experience might be relevant in trying to determine the cause of his fever?
    -  Where he went, how long, what he did, first/second gen emigrant traveller going back home (increased risk of travel-related morbidity due to increased exposure), exposures (unclean water, skin contact with unclean water, raw/uncooked foods, animals, birds, mozzies, ticks, flies, fleas, lice, mites, soil-skin, sexual, injections/body-piercings)
-  Does airline travel increase the risk of URTIs?
    -  Yes, stuck next to animals for extended period of time. 2003 SARS spread to 16 people from one infected on plane from HK to beijing
-  What serious or life-threatening illnesses (even if rare) present with fever, headache, and rash?
    -  Meningitis
    -  west nile
    -  jap encephalitis, tickborne encephalitis,
    -  Encephalitis
-  For which of these diseases could he have obtained preventative advice or vaccination?
    -  Do not recommend a vaccine based only on the destination country, because there is no single ‘correct’ list of vaccines for travel to any particular country. 3 types of vaccinations [ref](https://immunisationhandbook.health.gov.au/vaccination-for-special-risk-groups/vaccination-for-international-travellers)
        -  Routinely recommended (not travel spec): dtp, hbv, infleunza, pneumoccocal, mmr, varicella, meningococcal, polio
        -  Selected vaccines based on travel itinerary, activities, risk: cholera, hep a, jap encephalitis, rabies, tick-borne encephalitis, Tb, typhoid, yellow fever
        -  Required for entry into countries: yellow fever, Mecca, polio, shit changes
-  What diseases might he have been exposed to in the Sudan? How do you find out this sort of information?
    -  [Yellow book](https://wwwnc.cdc.gov/travel/page/yellowbook-home)
    -  Yellow fever, malaria
-  What general factors would you take into consideration at a pre-travel consultation? Should the same advice be given for every person travelling in the same group with the same itinerary? Explain.
    -  Personal info, age, pregnancy, finance
    -  PMHx, immuncompromised?, medications
    -  Vaccination history, allergies
    -  Purpose of travel, intended activities (esp assocated with environmental risks and hazards), date of departure and time for vaccinations, rural?, duration, access to health care?, how likely will they change it
    -  Travel insurance???
-  What further questions will you ask in relation to his current symptoms?
    -  Medical history: age, PSx, drugs, allergies, vaccinations, immune status (HIV, diabetes, pregnancy)
    -  Detailed account of travel history, destinations, activities, possible exposures, timeframe, season at destination
    -  Sequential history of current illness, associated symptoms, concurrent therapies, other people affected?

https://en.wikipedia.org/wiki/Remittent_fever

## Trigger 2: History

Richard tells you that the project he was working on is in the south of the Sudan, in a very poor area with many refugees living in awful conditions in temporary camps. There was flooding during his time there and there were rats everywhere. He says that disease was rife in the area, especially diarrhoea and malaria, but he had also heard that there were some measles cases reported and there was talk of the possibility of an epidemic.

Richard considers himself healthy, and there is certainly nothing of significance in his past medical history. He was quite well while he was away, apart from a couple of bouts of 'gastro', which he says was pretty much the normal for everyone on the project from time to time. He denies any sexual encounters during his trip.

Richard has lived in Cairns all his life with the exception of his time as a medical student at James Cook University in Townsville. He was fully vaccinated as a child, and the doctor he saw prior to leaving Australia gave him the recommended vaccinations for the Sudan. He was also given doxycycline as a malaria preventative, and he says he took this religiously while he was away, although he has not taken any since returning home.

You ask some more questions about his current illness. Richard tells you that the fever started quite suddenly five days ago. He has not had any rigors or bouts of profuse sweating. He had a ‘bit of a blotchy rash’ on the first day or so, but this settled quickly so he thought it was just a ‘heat rash’ due to his temperature. He felt generally very unwell and had a very severe headache ‘behind his eyes’.

The headache and the fever eased early yesterday, so he thought he was on the mend. This morning however he noticed a rash again (although it is slightly different this time), and his fever is back (although it is not as high). He still feels really unwell, and says he aches all over (‘everything hurts’). He is generally exhausted, off his food and slightly nauseated; but he has not vomited and has not noticed anything different in relation to his bowels or urine


-  What are the significant features of this history? Revisit your list of hypotheses. Which are most likely, and which are perhaps less likely but you need to ensure "not to miss"?
-  Is his failure to continue his malaria medication after leaving the area significant?
    - P. falciparum infection typically develops within one month of exposure (incubation period), thereby establishing the basis for continuing antimalarial prophylaxis for 4 weeks upon return from an endemic area.
-  What diseases are more common in conditions such as those described in the refugee camps? What factors contribute to the increased prevalence of these diseases?
-  Can you eliminate measles as a possibility at this point?
    -  Nope, maybe he wasn't immunised
-  What features of a rash would you describe to a colleague when discussing a case?
    -  Inspect: general obs, site and number of lesions, distn or configuration
    -  Describe the lesion: Size/shape, Colour, Associated secondary change, Morphology/margin OR if pigmented: Asymmetry, irregular Borders, two or more Colours, Diamater > 6mm
    -  distribution, configuration, colour, morphology, secondary lesions?
-  Does the time-frame of the illness so far give you any clues? What mechanisms might be responsible for the disappearance and reappearance of the fever and rash?
-  How might the fever and rash be related?
    -  Infection: local at the rash or spread from elsewhere e.g. vascular
    -  Dilation of dermal blood vessels + oedema + infiltration => papule (BLANCHING), haemorrhage (non-BLANCHING)



## Trigger 3: Physical Examination

At this point you are considering a number of potentially serious conditions amongst your differential diagnoses, and you hope the list will be reduced by the end of your physical examination of Richard.

General appearance: Richard looks generally unwell and miserable. He is slightly flushed, but his conjunctivae are not pale, and there is no evidence of jaundice. He appears well hydrated.

Vital signs: Temp 38C; BP 120/70; PR 80; RR 18/min; JVP normal

No neck stiffness. No abnormal neurological signs.

Rash: maculo-papular rash on trunk and limbs. No obvious insect bites or scratches. A few scattered petechiae are noted on his limbs and chest.

No lymphadenopathy; liver and spleen not palpable.

The remainder of your routine examination is normal. In particular, there is no other evidence of abnormal bleeding. In view of the small number of petechiae found, you decide to do a tourniquet test. After a brief explanation to Richard, you reapply and inflate the blood pressure cuff to above his diastolic pressure, and leave it in place for five minutes. At the end of this time you note an increase in the number of petechiae in his forearm distal to the cuff. The count is 23 petechiae in a 2.5cm2 area. In spite of the lack of specificity of the test, in view of the clinical picture you consider this significant.

You send Richard off for some blood tests, instructing him to return in two days, or immediately if he becomes worse or develops any other symptoms.


Discussion starters:
-  What are the significant features from this examination?
    -   A positive tourniquet test is an indicator of capillary fragility (vasculitis, connective tissue disorders, scurvy, cushings)
    -   Pump to between diastolic and systolic for 5mins then observe more than 20 petechiae in an area of 2.5cm2 is positive
-  Are you able to remove any of your hypotheses on the basis of this new information? In particular, can you eliminate malaria or meningitis?
-  What tests will you order? Justify your responses.
    -  Dengue: RT-PCR or NS1 first 7 days, from day 5 onwards dengue serology more likely to be positive, from day 10 negative serology virtually excludes
-  Is a lumbar puncture indicated? If not, why not?
-  What are petechiae, and what pathophysiologic process do they indicate?
    -  Platelet dysfn
    -  1-3mm
    -  Purpura: 3-10mm
    -  Ecchymoses >1cm

GAS: cellulitis, strep throat, post strep GN, scarlet fever

GBS: neonatal meningitis, pregnant women in third trimester

## Trigger 4: Investigations and results

Richard returns two days later, as requested, for his test results. He is already feeling a little better. Full blood count is reported as follows (reference range in brackets):

Haemoglobin 146 g/L (135-175)
Haematocrit 0.45 (0.40-0.54)
RCC 5.4x1012/L (4.5-6.5)
MCV 83 fL (80-100)
WCC 1.4 x109/L (3.5-10.0)
Neutrophils 0.8 x109/L (1.5-6.5)
Lymphocytes 0.5 x109/L (1.0-4.0)
Monocytes 0.1 x109/L (0-0.9)
Eosinophils 0 x109/L (0-0.6)
Basophils 0 x109/L (0-0.15)
Platelets 45 x109/L (150-400)

Serum chemistry: mildly elevated liver enzymes (AST > ALT)
Thick and thin films for malaria are negative.

Serology:
-  Dengue IgM and IgG positive. NS1 antigen test positive.
-  Measles IgG positive and IgM negative. Throat swab PCR and urine PCR for measles
are negative.
-  Parvovirus IgG and IgM negative.
-  Leptospirosis IgG and IgM negative.


Discussion starters:
-  What are the mechanisms by which neutropenia, lymphopenia, and thrombocytopenia can develop?
    -  Neutropenia: mild (1000-1500), moderate (500-1000), severe (less than 500)
        -  Acute rapid use
        -  Acute impaired production
        -  Chronic reduced production
        -  Chronic sequestration
    -  Lymphopenia
        -  Acute: viral, fasting, stress, steroids, chemo
        -  Chronic: autoimmune, chronic infn (AIDS, miliary tb), Ca
    -  Thrombocytopenia
        -  Bone marrow does not produce enough: leukemia, lymphoma, aplastic anaemia, alc, chemo drugs
        -  Spleen sequestration: cirrhosis, myelofibrosis
        -  Increased use/destruction: immune thrombocytopenia, HIV, drugs, DIC, TTP
        -  Shit platelets:von willebrand, aspirin
-  Can you exclude malaria on the basis of the negative smear? If not, why not?
    -  Could be latent in liver, bad lab
-  Is a single estimate of immunoglobulins sufficient for a definitive diagnosis of dengue (or any other infection) to be made? Explain.
    - Positive dengue IgM: can be reactive in other flavivirus infections (not specific)
-  What is the natural history of dengue fever?
    -  Uncomplicated: rarely serious. Fever => normal => fever. Day after the fever goes, more likely to enter DHF due to cytokines and vessel permeability
    -  Dengue haemorrhagic fever: bad cardinal features are fever, increased vascular permeability, haemorrhagic manifestations (easy bruising, skin haemorrhages, bleeding at venepuncture), and marked thrombocytopenia (≤100e9/L), hypotension, narrow pulse pressure, haemoconcentration (haemocrit increased 20% above normal or evidence of increased cap permeability) aka INTRAVASCULAR FLUID DEPLETION
    -  Humidity favour adult mosquito survival, increased temp shortens time for mosquito to become infectious
    -  positive-sense RNA
-  How are you going to manage this patient now?
    -  Oral fluids
    -  Paracetamol: don't use aspirin because of antiplatelets
    -  No IM
    -  Warn to go hospital
    -  If they come back: slow cap refill => refer
    -  If DHF: IV crystalloids (normal, hartmans) are as effective as colloid at 10mL/kg/hour (max of 500mL/hour)


## Trigger 5: Management

You ask Richard to have two further blood tests 24 hours apart to rule out a co-infection with malaria.

Richard is surprised to discover that he has dengue fever, as he had escaped it during the Cairns epidemic in 1997-99, unlike several members of his family and he thought he must be immune.

Richard expresses concern about the risk of developing dengue haemorrhagic fever, which he recalls from med school is a possibility with a second infection with a different strain of dengue. You reassure him that this complication is very unlikely, but (trying not to frighten him too much!) stress that he must contact you, or go to the hospital immediately if he feels *cold and clammy, or unreasonably drowsy or irritable, starts bleeding from the nose or gums, develops abdominal pain or vomiting (particularly if he vomits blood), or has black bowel motions, has difficulty breathing or dark bloody-looking blotches appear on his skin*.

In the meantime, as there is no specific treatment you can offer him, you suggest he rests, keeps up his fluids, and takes paracetamol if he needs it for his fever or general aches and pains. You ask him to return in a week’s time for review, at which time you will decide if any further tests are necessary.

Richard sees you the following week, and is clearly much improved. His subsequent thick and thin blood films were both negative for malaria. However, for the next few weeks he continues to suffer from general fatigue, and his joints remain sore. You assure him this will pass with time.





Discussion starters:
-  Is there more than one strain of dengue virus? Does infection with one strain confer protection against other strains?
    -  4 serotypes DEN 1-4, Aedes aegypti
    -  Nope it doesn't. Sequential infn diff serotype => Ab develop after first infn enhance immune response during second => combine with second => target monocytes and macrophages
-  What pathophysiological mechanisms lie behind each of the 'warning symptoms' outlined to Richard?
    -  Number of target cells, monocytes and macrophages infected => destruction => release of inflamm mediators
        - Increased vascular permeability => leakage into extracellular spaces => shock (cold and clammy, drowsy, irritable, difficulty breathing)
        - Haemorrhage from thombocytopenia, vascular changes and DIC (bleeding, black bowel motions, dark blood-looking blotches in skin)
-  How great is the public health risk here? What action, if any, should be taken to protect the community?
-  Is dengue fever a notifiable disease in Queensland?
    -  [Yes; provisional and path Dx](https://www.health.qld.gov.au/clinical-practice/guidelines-procedures/diseases-infection/notifiable-conditions/list)
    -  Not a quarantinable diseases, of which Australia has 8 (cholera, highly pathogenic avian influenza (H5N1), plague, rabies, severe acute respiratory syndrome (SARS), smallpox, the viral haemorrhagic fevers, yellow fever).
