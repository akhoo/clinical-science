---
title: "Immunology"
author: "Aaron Khoo"
date: "`r format(Sys.Date(), '%d %B %Y')`"
output:
  pdf_document
documentclass: article
classoption: a4paper
fontsize: 11pt
urlcolor: blue
---


# Main case

SOB causes: resp > CVS > RBC > psychotic

Immunodeficiencies split by acquired or inherent

Anaemics won't become cyanotic even if poor sats

Infant diarrhoea: viral infections most common with rotavirus, norovirus, adenovirus

Kids with heart conditions can get recurrent resp due to L>R and increased pressure in lungs lead to wet and more infections

Retinoblastoma means more likely for osteosarcoma and pagets disease

cyanosis, anaemia , dehydration ,neck stiffness , lymphadenopathy ,rash

Meningitis: rash late sign, bulging fontanelles and projectiling vomit

T Cell: virus, fungi => but can cause b cell issues as its imp for activation
B cell: bacteria
Macro: bacterial catalase positive
Complement: neisseria meningitidis


Diarrhoea: osmotic, exudative, hypermotility, secretory, inflamm



Obtaining urine sample

-  0-2: suprapubic
-  3: catether
-  3+: ask them to piss


Chronic inflamm ==> IL1 IL6 and tnfalpha => liver stimulates hepcidin => iron trapping in macrophages and liver cells => anaemia

## Trigger 5

Normal CD19 (B cells) but low Ig, because no T cells to help stimulate

## Trigger 6

AR SCID would lead to increase in deoxyadenosin toxic to both B and T, hence more likely to be X-linked SCID

# Short case 1



# Short case 2
