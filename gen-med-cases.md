# Standardised clinical scenarios practice

1.  PDx + DDx
2.  Ix
3.  Interpret Ix
4.  Initial Mx
5.  Long term Mx

## Hypertension

38F admitted to hospital presenting with 3 day worsening headache. 24 hours prior to admission vision became blurred, and she noticed ankle swelling and oliguria. Only medical history of note is that she was diagnosed with hypertension during third trimester of her last pregnancy, Rx: rest + antihypertensive. Post-partum the antihypertensive was discontinued and she never followed up for this.

She lives at home, is married with 3 children aged 6 years and 2 years. Normally quite healthy. Drinks 2 standards a week and smokes 1/2 pack a day for 20 years.

O/E: oriented PPT, pale and clinically anaemic. Slight ankle oedema. BP 190/140 mmHG, JVP not elevated. Chest, heart, abdo otherwise normal. CNS Ex shows papilloedema, retinal haemorrhages and exudate in both eyes. Visual acuity reduced.

[Bloods](https://i.imgur.com/zXKNNfN.png)




## SOB (PE)

29F sudden onset R sided chest pain + SOB. Woke her from sleep. Exacerbated by deep inspiration and coughing. Slight non-productive cough. PMHx includes asthma controlled by salbutamol. No FHx of note. 3/52 prior went overseas to USA. No recent sick contacts. OCP for 4 years now.

O/E: afebrile, RR 24, JVP elevated 3 cm, BP 110/64, HR 128. Reduced expansion, percussion and vocal resonance normal and equal. Pleural rub heard over R lower zone. No added sounds.

[ECG](https://i.imgur.com/7UHkA3K.png)

[CXR](https://i.imgur.com/WWZQNBW.png)

## Acute weakness (stroke)

80M presents to ED with sudden onset weakness in R arm and leg. Was sitting drinking beer when weakness came on, and slurred speech. Symptoms still present. BIBA and the incident happened 2 hours ago. He is hypertensive and suffered MI 3 years ago, well controlled T2DM, used to smoke but quit 20 years ago with a 40 pack year history. Drinks a 1 standard a week.

O/E: appears unwell. PR 80 irregularly irregular, BP 170/98. HS pansystolic murmur typical of MR, abdo SNT. Neuro exam demonstrates global R weakness 3/5 with impaired sensation, hyperreflexia, and decreased tone.

[ECG](https://litfl-wpengine.netdna-ssl.com/wp-content/uploads/2018/08/ECG-Atrial-Fibrillation-coarse-flutter-waves-2-2.jpg)


## Anaemia (i.e. GI bleeding)

48M has 3/12 abdo pain. The pain is epigastric and intermittent, episodes last 30-60 mins often at night, and can wake him up. Usually improves after meals, however some meals such as curry make it worse. He is obese and has been feeling quite fatigued for some time now. He reports urine is fine, however has passed some black stools during this time. He is a smoker, with 30 pack years, no recent travel, is regularly taking warfarin for her AF.

Oriented, appears a bit pale, comfortable. HR 100 irregularly irregular, BP 150/80. Pallor of conjunctiva. Mild tenderness in the epigastrium. Rest of exam normal.

[Bloods](https://i.imgur.com/ZyjqLLH.png)


## Solutions

### Hypertension

DDx: intracranial bleeds (SAH, SDH), masses (abscess or malignancy), cluster/tension/migraines, seizures, MS

Ix: FBC (Hb), urinalysis (protein), CHEM20 (renal function, urea, creatinine, electrolytes), 24 hour cortisol (cushings), catecholamines (phaeochromocytoma), TFTs (hashimotos), renal USS (kidney size, RAS)

Initial Mx: lower the BP gradual rate over 24 h. Sudden can cause thrombosis and infarction.

Longer Mx:


### PE

DDx: PE, pneumonia, asthma, pneumothorax, pericarditis, MI

Ix: ECG (rule out MI, S1Q3T3 nonspecific except in massive PE), CXR (rule out pneumothorax/pneumonia), FBC (anaemia, WCC infn), ddimer, ELFTs (baseline renal function), CTPA or VQ scan if cannot CTPA (allergic to contrast or pregnant)

Initial Mx: O2 if sats bad,

Longer Mx:

### stroke

DDx: todds paresis,...

Ix:

Initial Mx:

Longer Mx: rehabilitation, discussions with family
